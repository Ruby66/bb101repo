# doctest: Deaf Grandma Test
# >> message_to_grandma
# => "Hello Grandma"
#
# doctest: continue with questions
# >> message_to_grandma
# => @question == @question.upcase
def message_to_grandma
  puts 'Type what you would like to say to Grandma'
  @question = gets.chomp
  return @question
end

if message_to_grandma != 'BYE'
  while @question != @question.upcase
    puts "HUH?! SPEAK UP, SONNY!\n"
    message_to_grandma
  end

  if @question != @question.upcase
    puts "HUH?! SPEAK UP, SONNY!\n"
    message_to_grandma
  elsif
    @question == @question.upcase then
    puts "NO, NOT SINCE #{rand(1938...1950)}!"
  end
end






