# Program will show your Present Working Directory, create a new Direcotry
# Change Directories, show what's in a Directory then remove the newly
# created Directory
puts Dir.pwd
Dir.mkdir("tmp")
puts Dir.getwd
Dir.chdir("./tmp")
puts Dir.getwd
# Next liine of code will  show what's inside of the Present Working Directory
Dir.foreach(".") {|x| puts "Got #{x}"}
Dir.chdir("..")
puts Dir.getwd
Dir.rmdir("tmp")
# Below will  show what's inside of the Present Working Directory
puts "\n\nCurrently in #{Dir.getwd}"
Dir.foreach(".") {|x| puts "Got #{x}"}