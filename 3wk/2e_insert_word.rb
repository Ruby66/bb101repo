#puts "2222222222222222222222222222222222222222222222222222"
#file_to_read = IO.readlines("3wk_2e_text.txt")
#file_to_read.each do |read_string|
 # puts read_string.gsub(/word/, "Inserted word")
#end

if ARGV.empty?
  puts "You need to provide the file name you would like to substitute"
  exit(0)
end
file_name = ARGV[0] || './3wk/2e_text.txt' # gets.chomp
result = ''
IO.readlines(file_name).each do |read_string|
  result << read_string.gsub(/\bword\b/, "inserted word")
end

IO.write(file_name, result)
