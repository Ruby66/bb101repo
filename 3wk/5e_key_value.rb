# doctest: Partition method returns...
# >> s = 'key=value'
# >> s.partition('=')
# => ["key", "=", "value"]

# doctest: setup
# >> s1 = s.partition('=').first
# >> s2 = s.partition('=').last

# doctest: s1 must equal 'key'
# >> s1 == 'key'
# => true
# doctest: s2 must equal 'value'
# >> s2 == 'value'
# => true
#
# doctest: Our Program
#          report_on_string "key=value"
# >> report_on_string(s)
# => "s1 = key
#     s2 = value"
def report_on_string(s)
  s1, _, s2 = s.partition('=')
  "s1 = #{s1}\ns2 = #{s2}"
end
if __FILE__ == $PROGRAM_NAME
  s = 'key=value'
  puts report_on_string(s)
end
