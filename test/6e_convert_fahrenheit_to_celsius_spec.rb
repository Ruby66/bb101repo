require_relative 'spec_helper'
require './1wk/6e_convert_fahrenheit_to_celsius'

describe 'convert' do
  it 'must convert -40 F to -40C' do
    convert(-40).must_equal -40
  end

  it 'must convert 0F to -17.7778C' do
    convert(0).round(6).must_equal -17.777778
  end

  it 'must convert 32F to 0C' do
    convert(32).must_equal 0
  end
end
