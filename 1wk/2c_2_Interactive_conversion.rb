# This program is that asks for input from user and will convert
# from one temperature unit to another based on user input

# doctest: Request user input for Temperature Unit
# >> "You choose to convert to: #{temp_unit}"
# => "You choose to convert to: F"

def convert_to_fahrenheit (_Celsius)
  (_Celsius * 9 / 5.0 ) + 32
end
def convert_to_celsius(_Fahrenheit)
  (_Fahrenheit - 32) * 5 / 9.0
end
def select_conversion(temp_unit)
  if temp_unit == "F" then
    convert_to_fahrenheit(@temp_to_convert)
  elsif temp_unit == "C" then
    convert_to_celsius(@temp_to_convert)
  else

  end
end

puts "Please enter the Temperature Unit you want to convert to:"
temp_unit = gets.chomp.upcase
puts "You choose to convert to: #{temp_unit}"

# doctest: Request user input for Temperature to convert
# >> "Your temperature to convert is: #{@temp_to_convert}"
# => "Your temperature to convert is: 212.0"
puts "Your temperature to convert is:"
@temp_to_convert = gets.chomp.to_f



#puts "your temperature is now: #{convert_to_fahrenheit(@temp_to_convert)}"
#puts "Your temperature is now Converted to: #{select_conversion(temp_unit)}#{temp_unit}"
puts "Your temperature is now Converted to: %.2f#{temp_unit}" % select_conversion(temp_unit)




