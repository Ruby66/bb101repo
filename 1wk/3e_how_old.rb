# Write a Ruby program that displays how old I am, if I am 979000000 seconds
# old. Display the result as a floating point (decimal) number to two decimal
# places (for example, 17.23). Note: To format the output to say 2 decimal
# places, we can use the Kernel's format method. For example, if x = 45.5678
# then format("%.2f", x) will return the string 45.57
#     doctest: convert seconds to years
#     >> (age_in_years 9.79e8).round(6)
#     => 31.043886
def age_in_years(seconds)
  minutes = seconds / 60.0
  hours = minutes / 60
  days = hours / 24
  days / 365
end

begin
  # This is given to us, we can not change it.
  age_in_seconds = 979000000
  puts "My age in Years is: %.2f" % age_in_years(age_in_seconds)
  printf "My age in years is: %.2f\n", age_in_years(age_in_seconds)
  puts sprintf "My age in years is: %.2f", age_in_years(age_in_seconds)
  puts format "My age in years is: %.2f", age_in_years(age_in_seconds)

  # If age were to be less than 2 digits , such as .10, the last 0(s) would be dropped
  puts "My age in years is: #{age_in_years(age_in_seconds).round(2)}"

  format_string = "Manual Substitution, simulating format!  My age in years is: ####"
  answer = format_string.sub("####", age_in_years(age_in_seconds).round(2).to_s)
  puts answer
end if __FILE__ == $PROGRAM_NAME

