age_in_seconds = 979000000.0

def age (age_in_seconds)
  age_min = age_in_seconds / 60
  age_hrs = age_min / 60
  age_days = age_hrs / 24
  age_years = age_days /365
end
puts "\nAge in Seconds: 979000000 \nMy Age in Years: " + "%.2f" % age(age_in_seconds)
