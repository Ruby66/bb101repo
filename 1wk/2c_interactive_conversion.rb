# Write a Ruby program to convert degrees Fahrenheit to Celsius using a
# a method. The method should return the result with precision of 2
# decimal places.
# doctest: convert Fahrenheit to Celsius
# >> convert(98.6)
# => 37.0
# doctest: matchup
# >> convert -40
# => -40
# doctest: boiling point
# >> convert 212
# => 100
# doctest: freezing point
# >> convert 32
# => 0
# doctest: Will always return a float
# >> (convert 98).round(4)
# => 36.6667
def convert_to_celsius(_Fahrenheit)
  (_Fahrenheit - 32) * 5 / 9.0
end
alias :convert :convert_to_celsius
if __FILE__ == $PROGRAM_NAME
  [
    -40,
    0,
    32,
    98,
    98.6,
    100,
    212
  ].each do |fahrenheit|
    puts "Your number %<Fahrenheit>.2fF converts to %<Celsius>.2fC." %
                      {:Fahrenheit => fahrenheit,    :Celsius => convert(fahrenheit)}
  end
end
