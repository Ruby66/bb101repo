# This program snippet calulates the number of minutes in a year
# It does not include Leap Years.
# doctest: minutes in a year
# >> 60 * 24 * 365
# => 525_600
def minutes_in_a_year
  60 * 24 * 365
end

puts "There are %.2f Minutes in a year!" % minutes_in_a_year
