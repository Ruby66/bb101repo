# This program will accept a year as input to a method
# to determine if it's a leap year or not. Then if it is
# it will calculate the number of minutes for a Leap Year
# or the number of minutes for a Non-Leap Year

def check_for_leap_year? (year_to_check)
  if year_to_check % 4 == 0 && !(year_to_check % 100 == 0) || year_to_check % 400 == 0 then
    puts "Your year: %d is a Leap Year" % year_to_check
    leap_year_minutes(year_to_check)
  else
    puts "Your year: #{year_to_check} is NOT a Leap Year"
    non_leap_year_minutes(year_to_check)
  end
end

def leap_year_minutes (year_to_check)
  leap_year_minutes = 366 * 24 * 60
  puts "There are %d minutes in your year" % leap_year_minutes
end

def non_leap_year_minutes(year_to_check)
  non_leap_year_minutes = 365 * 24 * 60
  puts "So, there are only %d minutes in this year" % non_leap_year_minutes
end

puts "Please enter the Year you'd like\nto check to see if it's\na Leap Year:"
year_to_check = gets.to_i
check_for_leap_year?(year_to_check)
