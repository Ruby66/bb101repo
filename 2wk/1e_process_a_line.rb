# This program will transverse the given string and separate
# the string at the \n (new line) character
welcome_to_forum_string = "Welcome to the forum.\nHere you can learn Ruby.\nAlong with other members.\n"
given_string = welcome_to_forum_string.split(/\n/)

line_number = 1
given_string.each do |string_line|
  puts "Line:#{line_number} " + string_line
  line_number += 1
end
