=begin
doctest: I can add in Ruby
>> 1 + 2
=> 3
=end

=begin
We will create a program that will be able to:
1) Greet the world
2) Greet someone personally
3) Greet someone else personally
4) Ask if someone is there
doctest: Hello World Greeting
>> hello
=> 'Hello World!'
doctest: I can Greet someone personally
>> hello 'Michael'
=> 'Hello Michael!'
doctest: I can greet someone else personally
>> hello 'Bob'
=> 'Hello Bob!'
doctest: Is someone there
>> hello('Victor', '?')
=> 'Hello Victor?'
=end
def hello(name='World', punctuation = '!')
  'Hello ' + name + punctuation
end
